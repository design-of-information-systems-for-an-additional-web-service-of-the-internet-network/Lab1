import { JsonPipe, NgClass, NgIf } from '@angular/common';
import { ChangeDetectionStrategy, Component, signal } from '@angular/core';
import { FormsModule, NgForm } from '@angular/forms';
import { Product } from '../../models';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    FormsModule,
    JsonPipe,
    NgClass,
    NgIf,
  ],
  selector: 'app-product-form',
  standalone: true,
  templateUrl: './product-form.component.html',
})
export class ProductFormComponent {
  public readonly form: Product = {
    brandId: '',
    categoryId: '',
    code: '',
    count: null,
    description: '',
    expectedAt: null,
    guaranty: false,
    image: null,
    name: '',
    price: null,
    status: null,
    trialPeriod: false,
  };
  public imagePreview = signal<string | null>(null);
  public visible = signal(false);

  public get minDate(): string {
    return new Date(new Date().setDate(new Date().getDate() + 1)).toJSON().split('T')[0];
  }

  public get maxDate(): string {
    const maxMonth = 3;
    const date = new Date().setMonth(new Date().getMonth() + maxMonth);

    return new Date(date).toJSON().split('T')[0];
  }

  public submit(): void {
    this.visible.set(true);
  }

  public reset(form: NgForm): void {
    form.resetForm();
    this.visible.set(false);

    if (this.imagePreview()) {
      URL.revokeObjectURL(this.imagePreview()!);
      this.imagePreview.set(null);
    }
  }

  public preview({ target }: Event): void {
    const files = (target as HTMLInputElement).files;

    if (files?.length) {
      this.imagePreview.set(URL.createObjectURL(files[0]));
    }
  }
}

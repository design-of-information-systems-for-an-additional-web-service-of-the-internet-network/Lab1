import { Routes } from '@angular/router';
import { ProductsContainer } from './products.container';

export const productsRoutes: Routes = [
  {
    path: '',
    component: ProductsContainer,
  },
];

import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ProductFormComponent } from './components/product-form';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    ProductFormComponent,
  ],
  standalone: true,
  templateUrl: './products.container.html',
})
export class ProductsContainer {}

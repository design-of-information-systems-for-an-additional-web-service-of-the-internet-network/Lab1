export interface Product {
  brandId: string;
  categoryId: string;
  code: string;
  count: number | null;
  description: string;
  expectedAt: string | null;
  guaranty: boolean;
  image: string | null;
  name: string;
  price: number | null;
  status: 'active' | 'inactive' | 'expected' | null;
  trialPeriod: boolean;
}
